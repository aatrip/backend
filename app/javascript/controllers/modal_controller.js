import { Controller } from "stimulus"

export default class extends Controller {
  open() {
    let modal = document.getElementById("modal");
    modal.classList.add("active");
  }

  hide() {
    let modal = document.getElementById("modal");
    modal.classList.remove("active");
  }

  populate() {
    console.log(this.data.get("mid_name"));
  }
}