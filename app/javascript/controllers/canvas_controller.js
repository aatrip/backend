import { Controller } from "stimulus"

export default class extends Controller {
  open() {
    let canvas = document.getElementById("canvas");
    canvas.classList.toggle("active");
  }
}