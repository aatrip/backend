import { Controller } from "stimulus"

export default class extends Controller {
  add_transit() {
    var addition = document.getElementsByClassName('addition');
    var button = document.getElementById('transit-btn');
    var transitLogo = document.getElementById('transit-logo');

    if(addition.length < 1) {
      var element = '<div class="addition"> <div class="row"> <div class="col-4"> <div class="field"> <label>Địa điểm</label> <input type="text" name="route[transit_place]"> </div> </div> <div class="col-4"> <div class="field"> <label>Thời gian đến</label> <input type="text" name="route[transit_timedepart]" data-controller="date" class="flatpickr-input" readonly="readonly"> </div> </div> <div class="col-4"> <div class="field"> <label>Thời gian đi</label> <input type="text" name="route[transit_timearrival]" data-controller="date" class="flatpickr-input" readonly="readonly"> </div> </div> </div> <div class="row"> <div class="col-6"> <div class="field"> <label>Thời gian từ điểm đi đến điểm quá cảnh</label> <input type="text" name="route[transit_time_ab]"> </div> </div> <div class="col-6"> <div class="field"> <label>Thời gian từ điểm quá cảnh đến điểm đến</label> <input type="text" name="route[transit_time_bc]"> </div> </div> </div> <div class="row"> <div class="col-6"> <div class="field"> <label>Tên hãng bay</label> <input type="text" name="route[transit_airline_name]" required> </div> </div> <div class="col-6"> <div class="field"> <label>Số hiệu chuyến bay</label> <input type="text" name="route[transit_airline_id]" required> </div> </div> </div> </div>';
      var parent = document.getElementById('transit');

      parent.insertAdjacentHTML('afterend', element);
      button.innerText = 'Tắt quá cảnh';
      button.classList.remove('secondary');
      button.classList.add('btn-export');
      transitLogo.classList.remove('hide');
    } else {
      button.innerText = 'Bật quá cảnh';
      button.classList.add('secondary');
      button.classList.remove('btn-export');
      transitLogo.classList.add('hide');
      while(addition.length > 0){
        addition[0].parentNode.removeChild(addition[0]);
      }
    }
  }
}