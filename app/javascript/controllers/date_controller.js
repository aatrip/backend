import Flatpickr from "stimulus-flatpickr";

// create a new Stimulus controller by extending stimulus-flatpickr wrapper controller
export default class extends Flatpickr {
  connect() {
    //define locale and global flatpickr settings for all datepickers
    this.config = {
      enableTime: true,
      time_24hr: true,
      allowInput: true,
      dateFormat: "Y-m-d H:i"
    };

    super.connect();
  }
}
