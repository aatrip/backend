import { Controller } from "stimulus"

export default class extends Controller {
  connect() {
    setTimeout(() => this.alert_close(), 3000)
  }

  alert_close() {
    this.element.outerHTML = ""
  }
}