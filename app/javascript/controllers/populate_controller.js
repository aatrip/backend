import { Controller } from "stimulus"

export default class extends Controller {
  add() {
    parent.document.getElementById("passenger_full_name").value = this.data.get("full_name")
    parent.document.getElementById("passenger_nid").value = this.data.get("nid")
    parent.document.getElementById("passenger_pid").value = this.data.get("pid")
    parent.document.getElementById("passenger_phone_number").value = this.data.get("phone_number")
    parent.document.getElementById("passenger_dob").value = this.data.get("dob")
    parent.document.getElementById("passenger_dob").nextSibling.value = this.data.get("dob")

    const resultsArea = document.querySelector(".search-results");
    resultsArea.classList.remove("showing");
  }
}