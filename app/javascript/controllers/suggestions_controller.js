import { Controller } from "stimulus";

export default class extends Controller {
  static targets = ["searchResults", "searchItem"];

  requestSearch() {
    const resultsArea = document.querySelector(".search-results");
    const outside = document.querySelector("body");

    if (this.searchItemTarget.value.length > 2) {
      resultsArea.classList.add("showing");

      setTimeout(() => {
        fetch("/suggestions?name=" + this.searchItemTarget.value)
        .then(response => {
          return response.text();
        })
        .then(html => {
          resultsArea.innerHTML = html;
        });
      }, 500);
    } else {
      resultsArea.classList.remove("showing");
    }

    outside.addEventListener("click", function(){
      resultsArea.classList.remove("showing");
    });
  }
}
