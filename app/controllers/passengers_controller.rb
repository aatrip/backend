class PassengersController < ApplicationController
  before_action :set_ticket
  before_action :set_ticket_passenger, only: [:edit, :update, :destroy]

  # GET /passengers/new
  def new
    @passenger = @ticket.passengers.new
  end

  # GET /passengers/1/edit
  def edit
  end

  # POST /passengers
  def create
    @passenger =  @ticket.passengers.new(passenger_params)

    if @passenger.save
      redirect_to ticket_path(@ticket), notice: 'Hành khách đã thêm thành công'
    else
      redirect_to ticket_path(@ticket), notice: 'Đã có lỗi xảy ra, hãy kiểm tra lại thông tin'
    end
  end

  # PATCH/PUT /passengers/1
  def update
    if @passenger.update(passenger_params)
      redirect_to ticket_path(@ticket), notice: 'Hành khách đã được cập nhật thành công'
    else
      redirect_to ticket_path(@ticket), notice: 'Đã có lỗi xảy ra, hãy kiểm tra lại thông tin'
    end
  end

  # DELETE /passengers/1
  def destroy
    @passenger.destroy
    redirect_to ticket_path(@ticket), notice: 'Đã xóa hành khách'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:ticket_id])
    end

    def set_ticket_passenger
      @passenger = @ticket.passengers.find_by!(id: params[:id]) if @ticket
    end

    # Only allow a trusted parameter "white list" through.
    def passenger_params
      params.require(:passenger).permit(:full_name, :dob, :phone_number, :nid, :pid, :seat_type, :ticket_class, :extra, :more_extra, :user_id)
    end
end
