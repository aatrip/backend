class ExportController < ApplicationController
  skip_before_action :authenticate_user!
  
  layout 'pdf'

  def show
    @ticket = Ticket.find(params[:ticket_id])
  end
end
