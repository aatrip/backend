class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]

  # GET /tickets/1
  def show
    @airlines = @ticket.airlines
    @routes = @ticket.routes
    @passengers = @ticket.passengers
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new
  end

  # POST /tickets
  def create
    @ticket = Ticket.new(ticket_params)

    if @ticket.save
      redirect_to ticket_path(@ticket), notice: 'Hóa đơn đã được tạo thành công'
    else
      render :new
    end
  end

  # PATCH/PUT /tickets/1
  def update
    if @ticket.update(ticket_params)
      redirect_to ticket_path(@ticket), notice: 'Hóa đơn đã được cập nhật thành công'
    else
      render :edit
    end
  end

  # DELETE /tickets/1
  def destroy
    @ticket.destroy
    redirect_to root_url, notice: 'Đã xóa hóa đơn'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ticket_params
      params.require(:ticket).permit(:airline_name, :airline_id, :tid, :booking_date, :ticket_type, :airline_logo, :note)
    end
end
