class RoutesController < ApplicationController
  before_action :set_ticket
  before_action :set_ticket_route, only: [:edit, :update, :destroy]

  # GET /routes/new
  def new
    @route = @ticket.routes.new
  end

  # GET /routes/1/edit
  def edit
  end

  # POST /routes
  def create
    @route =  @ticket.routes.new(route_params)

    if @route.save
      redirect_to ticket_path(@ticket), notice: 'Chặng bay đã tạo thành công'
    else
      redirect_to ticket_path(@ticket), notice: 'Đã có lỗi xảy ra, hãy kiểm tra lại thông tin'
    end
  end

  # PATCH/PUT /routes/1
  def update
    if @route.update(route_params)
      redirect_to ticket_path(@ticket), notice: 'Chặng bay đã cập nhật thành công'
    else
      redirect_to ticket_path(@ticket), notice: 'Đã có lỗi xảy ra, hãy kiểm tra lại thông tin'
    end
  end

  # DELETE /routes/1
  def destroy
    @route.destroy
    redirect_to ticket_path(@ticket), notice: 'Đã xóa chặng bay'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:ticket_id])
    end

    def set_ticket_route
      @route = @ticket.routes.find_by!(id: params[:id]) if @ticket
    end

    # Only allow a trusted parameter "white list" through.
    def route_params
      params.require(:route).permit(
        :depart_time,
        :depart_place,
        :arrival_time,
        :arrival_place,
        :transit_place,
        :transit_timedepart,
        :transit_timearrival,
        :transit_airline_name,
        :transit_airline_logo,
        :transit_airline_id,
        :duration,
        :transit_time_ab,
        :transit_time_bc
      )
    end
end
