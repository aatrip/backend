class ManagePassengersController < ApplicationController
  before_action :set_passenger, only: [:edit, :update, :destroy]

  layout 'pdf', only: [:search]

  # GET /passengers
  def index
    if current_user.user_role == 'administrator'
      @results = Passenger.passenger_search(params[:q]).paginate(page: params[:page], per_page: 20)
      @passengers = Passenger.all.order('created_at ASC').paginate(page: params[:page], per_page: 20)
    else
      @results = Passenger.where(user_id: current_user).passenger_search(params[:q]).paginate(page: params[:page], per_page: 20)
      @passengers = Passenger.all.order('created_at ASC').where(user_id: current_user).paginate(page: params[:page], per_page: 20)
    end
  end

  def search
    @results = Passenger.where(user_id: current_user).passenger_search(params[:q])
    @pass_contact = Passenger.all.order('created_at DESC').where(user_id: current_user).paginate(page: params[:page], per_page: 5)
  end

  def suggest
    @names = Passenger.where(user_id: current_user).passenger_search(params[:name])

    render layout: false
  end

  def new
    @passenger = Passenger.new
  end

  def create
    @passenger = Passenger.new(passenger_params)

    if @passenger.save
      redirect_to manage_passengers_path, notice: 'Hành khách đã được thêm thành công'
    else
      render :new
    end
  end

  # GET /passengers/1/edit
  def edit
  end

  # PATCH/PUT /passengers/1
  def update
    if @passenger.update(passenger_params)
      redirect_to manage_passengers_path, notice: 'Hành khách đã được cập nhật thành công'
    else
      render :edit
    end
  end

  def destroy
    @passenger.destroy
    redirect_to manage_passengers_url, notice: 'Đã xóa hành khách'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_passenger
      @passenger = Passenger.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def passenger_params
      params.require(:passenger).permit(:full_name, :dob, :phone_number, :nid, :pid, :seat_type, :ticket_class, :extra, :user_id, :ticket_id)
    end
end
