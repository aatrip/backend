class AirlinesController < ApplicationController
  before_action :set_ticket
  before_action :set_ticket_airline, only: [:edit, :update, :destroy]

  # GET /airlines/new
  def new
    @airline = @ticket.airlines.new
  end

  # GET /airlines/1/edit
  def edit
  end

  # POST /airlines
  def create
    @airline =  @ticket.airlines.new(airline_params)

    if @airline.save
      redirect_to ticket_path(@ticket), notice: 'Hãng bay đã tạo thành công'
    else
      redirect_to ticket_path(@ticket), notice: 'Đã có lỗi xảy ra, hãy kiểm tra lại thông tin'
    end
  end

  # PATCH/PUT /airlines/1
  def update
    if @airline.update(airline_params)
      redirect_to ticket_path(@ticket), notice: 'Hãng bay đã cập nhật thành công'
    else
      redirect_to ticket_path(@ticket), notice: 'Đã có lỗi xảy ra, hãy kiểm tra lại thông tin'
    end
  end

  # DELETE /airlines/1
  def destroy
    @airline.destroy
    redirect_to ticket_path(@ticket), notice: 'Đã xóa hãng bay'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:ticket_id])
    end

    def set_ticket_airline
      @airline = @ticket.airlines.find_by!(id: params[:id]) if @ticket
    end

    # Only allow a trusted parameter "white list" through.
    def airline_params
      params.require(:airline).permit(:name, :aid, :logo)
    end
end
