module ApplicationHelper
  def is_active?(link_path)
    "active" if request.fullpath.include?(link_path)
  end

  def export_date(date)
    date = Time.parse(date)
    date.strftime('%A, %d %b %Y')
  end
end
