class Airline < ApplicationRecord
  has_one_attached :logo
  
  belongs_to :ticket

  validate :only_two_airlines

  validates :name, length: { in: 1..180 }
  validates :aid, length: { in: 1..180 }

  private
    def set_ticket
      ticket = Ticket.find(self.ticket_id)
    end

    # only allow user create two airlines
    def only_two_airlines
      if ticket.airlines.count > 1
        errors.add :base, 'Bạn không thể tạo thêm hãng bay' 
      end
    end
end
