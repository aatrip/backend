class User < ApplicationRecord
  devise :database_authenticatable,
         :rememberable, :validatable

  has_many :passengers
  
  enum user_role: %i[staff administrator]

  def destroy
    update_attributes(deactivated: true) unless deactivated
  end

  def active_for_authentication?
    super && !deactivated
  end
end
