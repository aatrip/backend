class Passenger < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :passenger_search,
    against: [:full_name, :phone_number, :dob, :nid, :pid],
    using: {
      tsearch: {
        prefix: true,
        negation: true
      }
    }

  belongs_to :ticket, optional: true
  belongs_to :user

  enum seat_type: %i[adt chd inf]
  enum ticket_class: %i[biz eco promo plus lite]

  validates :full_name, presence: true, length: { in: 1..180 }
  validates :phone_number, presence: true, length: { in: 1..180 }
  validates :dob, presence: true
end
