class Route < ApplicationRecord
  include ActionView::Helpers::DateHelper
  has_one_attached :transit_airline_logo

  belongs_to :ticket

  store_accessor :transit, :transit_place, :transit_timedepart, :transit_timearrival, :transit_duration, :transit_airline_name, :transit_airline_id, :transit_time_ab, :transit_time_bc

  validates :depart_time, presence: true
  validates :depart_place, presence: true, length: { in: 1..254 }
  validates :arrival_time, presence: true
  validates :arrival_place, presence: true, length: { in: 1..254 }

  before_create :set_duration

  private
    # set duration before create record
    def set_duration
      if self.transit_timedepart.present? && self.transit_timearrival.present?
        self.transit_duration = distance_of_time_in_words(self.transit_timedepart, self.transit_timearrival, :only => [:days, :hours, :minutes])
      end
    end
end
