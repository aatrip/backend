class Ticket < ApplicationRecord
  has_rich_text :note

  has_many :airlines, dependent: :destroy
  has_many :routes, dependent: :destroy
  has_many :passengers, dependent: :destroy

  enum ticket_type: %i[oneway roundtrip]

  validates :tid, length: { in: 1..180 }
  validates :ticket_type, presence: true, inclusion: { in: Ticket.ticket_types.keys }
end
