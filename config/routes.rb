Rails.application.routes.draw do
  devise_for :users

  root to: 'dashboard#index'
  
  resources :tickets, except: [:index, :edit] do
    get '/export', to: 'export#show'

    resources :airlines, except: [:index, :show]  
    resources :routes, except: [:index, :show]  
    resources :passengers, except: [:index, :show]
  end

  resources :users
  
  resources :manage_passengers do
    collection do
      get '/search', to: 'manage_passengers#search'
    end
  end

  get '/suggestions', to: 'manage_passengers#suggest'
end
