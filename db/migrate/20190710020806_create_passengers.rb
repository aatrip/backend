class CreatePassengers < ActiveRecord::Migration[6.0]
  def change
    create_table :passengers do |t|
      t.string :first_name, null: false, default: ""
      t.string :last_name, null: false, default: ""
      t.string :mid_name, null: false, default: ""
      t.string :full_name
      t.datetime :dob, null: false, default: ""
      t.string :phone_number, null: false, default: ""
      t.string :nid
      t.string :pid
      t.references :ticket, null: false, foreign_key: true

      t.timestamps
    end
  end
end
