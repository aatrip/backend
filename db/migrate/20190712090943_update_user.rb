class UpdateUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :deactivated, :boolean
    add_column :users, :user_role, :integer, default: 0
  end
end
