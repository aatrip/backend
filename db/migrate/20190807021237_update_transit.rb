class UpdateTransit < ActiveRecord::Migration[6.0]
  def change
    remove_column :routes, :has_transit
    add_column :routes, :transit, :jsonb, default: {}
  end
end
