class CleanUpRoute < ActiveRecord::Migration[6.0]
  def change
    remove_column :routes, :transit_time
    add_column :routes, :has_transit, :boolean, :default => false
  end
end
