class CleanUp < ActiveRecord::Migration[6.0]
  def change
    remove_column :tickets, :booking_date
    remove_column :passengers, :first_name
    remove_column :passengers, :last_name
    remove_column :passengers, :mid_name
  end
end
