class CreateRoutes < ActiveRecord::Migration[6.0]
  def change
    create_table :routes do |t|
      t.string :depart_time, null: false, default: ""
      t.string :depart_place, null: false, default: ""
      t.string :arrival_time, null: false, default: ""
      t.string :arrival_place, null: false, default: ""
      t.string :duration
      t.references :ticket, null: false, foreign_key: true

      t.timestamps
    end
  end
end
