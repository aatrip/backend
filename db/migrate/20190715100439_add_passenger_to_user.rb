class AddPassengerToUser < ActiveRecord::Migration[6.0]
  def change
    add_reference :passengers, :user, index: true
  end
end
