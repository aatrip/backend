class CreateAirlines < ActiveRecord::Migration[6.0]
  def change
    create_table :airlines do |t|
      t.string :name, null: false, default: ""
      t.string :aid, null: false, default: ""
      t.references :ticket, null: false, foreign_key: true

      t.timestamps
    end
  end
end
