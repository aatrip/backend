class CreateTickets < ActiveRecord::Migration[6.0]
  def change
    create_table :tickets do |t|
      t.string :airline_name, null: false, default: ""
      t.string :airline_id, null: false, default: ""
      t.string :tid, null: false, default: ""
      t.string :booking_date, null: false, default: ""

      t.timestamps
    end
  end
end
