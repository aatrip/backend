class AddTransitTimeToRoute < ActiveRecord::Migration[6.0]
  def change
    add_column :routes, :transit_time, :string
  end
end
