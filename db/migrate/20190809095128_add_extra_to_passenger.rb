class AddExtraToPassenger < ActiveRecord::Migration[6.0]
  def change
    add_column :passengers, :more_extra, :string, default: ""
    change_column :passengers, :extra, :string
  end
end
