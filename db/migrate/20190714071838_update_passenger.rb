class UpdatePassenger < ActiveRecord::Migration[6.0]
  def change
    add_column :passengers, :seat_type, :integer, null: false, default: 0
    add_column :passengers, :ticket_class, :integer, null: false, default: 0
    add_column :passengers, :extra, :float, default: 0.0
  end
end
